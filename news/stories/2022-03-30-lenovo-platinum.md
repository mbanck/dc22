---
title: Lenovo Platinum Sponsor of DebConf22
---

We are very pleased to announce that [**Lenovo**](https://www.lenovo.com/)
has committed to supporting [DebConf22](https://debconf22.debconf.org) as a
**Platinum sponsor**. This is the fourth year in a row that Lenovo is
sponsoring The Debian Conference with the higher tier!

As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, [**Lenovo**](https://www.lenovo.com)
understands how critical open systems and platforms are to a connected world.

With this commitment as Platinum Sponsor, Lenovo is contributing to make
possible our annual conference, and directly supporting the progress of Debian
and Free Software, helping to strengthen the community that continues to
collaborate on Debian projects throughout the rest of the year.

Thank you very much Lenovo, for your support of DebConf22!

## Become a sponsor too!

[DebConf22](https://debconf22.debconf.org) **will take place from July 17th to
24th, 2022 at the [Innovation and Training Park (ITP)](https://itp-prizren.com)
in Prizren, Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

And DebConf22 is still accepting sponsors!
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf22 website at
[https://debconf22.debconf.org/sponsors/become-a-sponsor](https://debconf22.debconf.org/sponsors/become-a-sponsor).

