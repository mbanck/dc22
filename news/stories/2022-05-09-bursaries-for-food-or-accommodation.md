---
title: DebConf22 bursaries for food or accommodation
---

If you missed the deadline to ask for DebConf22 bursaries, we have a good news:
you can still ask for food and/or accommodation.

It is not possible to ask for a bursary through the registration form now, but
if you are interested, please send an e-mail to bursaries@debconf.org saying if
you need:

- Food only
- Accommodation only
- Food + Accommodation

for the specific dates you plan to attend, and specify what you intend to do at
DebConf22.

We are no longer accepting requests for travel bursary.
