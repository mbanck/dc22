---
title: DebConf22 will be an in-person conference
---


The DebConf team has decided to hold the 2022 edition as an in-person conference!

Considering the COVID19 pandemic situation has hugely improved, we saw fit to meet our members and contributors face-to-face this year, after two years of online editions.
The Innovation & Training Park in Prizren, Kosovo awaits Debian people to gather and fill the place with the spirit of free software!

Currently, Kosovo authorities allow conferences to be held and the DebConf venue fulfills their requirements.
As of now, [requirements](https://msh.rks-gov.net/wp-content/uploads/2022/03/Vendimi-per-masat-e-reja-kunder-Covid-19-01-mars-2022-ENG.doc) for entry to the country are one of:

  1. Complete COVID-19 vaccination with the most recent dose within the last 12 months,
  2. A COVID-19 booster-shot after complete vaccination,
  3. Recovery from COVID-19, within 90 days, with PCR test proof, or
  4. A negative PCR test taken less than 48 hours before arrival.
  
The local team will ensure the enforcement of the local regulations related to the COVID19 pandemic. We will also offer rapid antigen tests and area for quarantining.

Let's meet in person during Debcamp 10 - 16 July and for the talks, workshops and BoFs during DebConf 17 - 24 July.

Registration and the CfP will open soon. Stay tuned!
