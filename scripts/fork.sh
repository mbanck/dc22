#!/bin/sh
set -euf

name=$1

git clone --single-branch --no-local . ../$name/
cd ../$name/
git branch
git filter-repo --invert-paths \
      --path-glob "assets/docs/*.pdf" \
      --path-glob "assets/img/*.jpg" \
      --path-glob "assets/img/*.png" \
      --path-glob "assets/img/*.svg" \
      --path-glob "assets/img/favicon/*.ico" \
      --path-glob "assets/img/favicon/*.png" \
      --path-glob "assets/img/favicon/*.svg" \
      --path-glob "assets/video/*.jpg" \
      --path-glob "assets/video/*.mp4" \
      --path-glob "sponsors/*.png" \
      --path-glob "sponsors/*.svg" \
      --path-glob "sponsors/*.yml" \
      --path-glob "static/"
  ' HEAD
git remote rm origin
rm -rf .git/refs/remotes
git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
git reflog expire --expire=now --all
git gc --prune=now --aggressive
