---
name: Travel through neighbouring countries
---

# Travel through neighbouring countries -  COVID-19 regulations

If you are interested on how to get to Prizren from neighbouring countries, 
see the [venue page](/about/venue).
## Albania

As of March 2022:

Starting from 06th of September 2021, to enter the Republic of Albania,
 whether by land, sea or air, you must be equipped with:

* Vaccination passport, which is valid 14 days after the second dose of
  vaccine has been administered; OR
* PCR test performed up to 72 hours before entry; OR
* Rapid antigen test performed within 48 hours before entry; OR
* Valid document proving that you have passed or recovered from Covid-19
  up to 6 months after recovery;
* These criteria also applies to those travelers who transit through
  Albania;
* Children up to the age of 6 are exempt from these criteria;
* Albanian citizens residing in Albania, who return and do not have a
  document mentioned above, must be isolated for 10 days and inform the
  health authorities to perform a test at the end of isolation.

## North Macedonia

As of March 2022:

The CDC has issued a Level 4 Travel Health Notice for North Macedonia
due to COVID-19, indicating a very high level of COVID-19 in the
country.
The risk of contracting COVID-19 and developing severe symptoms may be
lower if travelers are fully vaccinated with an FDA authorized vaccine.
Before planning any international travel, please review the CDC’s
specific recommendations for fully vaccinated and unvaccinated
travelers.

All travelers ages eighteen and older entering North Macedonia must
provide one of the following documents:

* Vaccination certificate with two doses of the COVID-19 vaccine;
* Negative PCR test taken within 72 hours prior to travel;
* Negative rapid antigen test taken within 48 hours prior to travel; or
* Certificate of COVID-19 recovery issued within the previous 180 days,
  counting from the day of recovery.

Foreign travelers transiting through North Macedonia will be allowed to
remain within the country borders up to five hours and must sign a
statement collected by the Border Police.

Citizens of North Macedonia and foreign citizens ages eighteen and older
entering the country without a COVID-19 vaccination certificate, PCR
test, antigen test, or certificate of recovery from COVID-19 must
quarantine at home for seven days at their own expense.
The quarantine period can be shortened to five days after presenting a
negative PCR test taken after day five.

The government has mandated a 14-day quarantine period for all travelers
arriving from India to prevent the spread of a COVID-19 variant
identified in this country.
The government will reassess this measure based on the epidemiological
situation in the country.

The [Institute of Public Health’s webpage](https://www.iph.mk/en/) and
the [Ministry of Health’s Facebook
page](https://www.facebook.com/iph.mk/) provide daily updates in
Macedonian, Albanian, and English on the number of reported cases,
fatalities, and recoveries.

To learn more about the measures adopted by the Government of North
Macedonia to prevent the spread of COVID-19 in the country, please check
the government website.

## Serbia

As of March 2022:

ENTRY REGIME FOR FOREIGN CITIZENS

Foreign citizens are allowed to enter the Republic of Serbia provided
that they have one of the following documents:

* Negative RT-PCR test for the presence of SARS-CoV-2 virus while the
  negative Antigen FIA Rapid test is valid only if it is done in the
  United States of America or Republic of Slovenia, not older than 48
  hours from the date of issue of the results (if there is a delay in
  entering the Republic of Serbia, which cannot be attributed to the
  passenger's fault, but is a consequence of an external event that
  could not have been foreseen, avoided or eliminated – delay of the
  flight, the departure of buses, trains, etc, then the test cannot be
  older than 72 hours when entering the Republic of Serbia);
* Certificate of complete vaccination issued by the Republic of Serbia,
  i.e. a foreign state with which the Republic of Serbia has concluded
  an agreement on the recognition of vaccination (those are: Greece,
  Hungary, Romania, Slovenia*, Turkey, United Arab Emirates, Czech
  Republic, India) or with which there is de facto reciprocity in the
  recognition of the vaccination certificate (those are: Andorra,
  Armenia, Austria, Belgium, Cabo Verde, Cambodia, Croatia, Cuba,
  Cyprus, Egypt, Estonia, Finland, France, Georgia, Germany, Iceland,
  Ireland, Italy, Lebanon, Liechtenstein, Lithuania, Malta, Mauritania,
  Morocco, Moldova, Netherlands, San Marino, Slovakia, Spain,
  Switzerland, Tunisia, United Kingdom of Great Britain and Northern
  Ireland, Ukraine);
* Certificate of overcome COVID-19 disease – a certificate or other
  public document stating that the holder of the document has overcome
  the disease caused by the SARS-CoV-2 virus, i.e. that this person has
  been diagnosed with the SARS-CoV-2 virus, provided that no less than
  14 days or more than six months may elapse from the first test, issued
  by the Institute of Public Health established for the territory of the
  Republic of Serbia, i.e. the competent authority of the state with
  which the Republic of Serbia has concluded an agreement or de facto
  reciprocity on the recognition of such documents (those are: Andorra,
  Austria, Bulgaria, Greece, Denmark, Croatia, Estonia, Ireland,
  Iceland, Cabo Verde, Liechtenstein, Luxembourg, Germany, Romania, San
  Marino, United States of America, Slovenia, Tunisia, Turkey,
  Switzerland, Spain);
* EU digital certificate issued by the competent authorities of states
  in the EU Digital COVID certificate system, regarding the trust
  framework for issuing, verifying and recognizing interoperable
  vaccination certificates of vaccination against COVID-19, tests and
  recovery certificates (EU Digital COVID Certificate).
* Beside EU member states, the above mentioned applies also to EU
  digital certificate issued by the competent authorities of [countries
  outside EU that are part of the EU Digital COVID certificate
  system](https://ec.europa.eu/info/live-work-travel-eu/coronavirus-response/safe-covid-19-vaccines-europeans/eu-digital-covid-certificate_en#recognition-of-covid-certificates-from-third-non-eu-countries>).

## Montenegro

As of March 2022:

According to media reports on Thursday (10 March), Montenegro lifted all
entry restrictions on 11 March, allowing entry without proof of a
negative test, vaccination or recovery.
In addition, proof is also no longer required for access to numerous
areas of public life, such as sports, cultural and recreational
facilities.

